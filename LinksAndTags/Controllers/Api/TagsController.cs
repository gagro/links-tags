﻿using AutoMapper;
using LinksAndTags.Models;
using LinksAndTags.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinksAndTags.Controllers.Api
{
    [Route("/api/links/{linkName}/tags")]
    [Authorize]
    public class TagsController : Controller
    {
        private ILinksAndTagsRepository _repository;
        private ILogger<TagsController> _logger;

        public TagsController(ILinksAndTagsRepository repository, ILogger<TagsController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("")]
        public IActionResult Get(string linkName)
        {
            try
            {
                var link = _repository.GetUserLinkByName(linkName, User.Identity.Name);
                return Ok(Mapper.Map<IEnumerable<TagViewModel>>(link.Tags));
            }
            catch (Exception ex)
            {

                _logger.LogError("Failed to get tags; {0}", ex);
            }

            return BadRequest("Failed to get tags");
        }

        [HttpPost("")]
        public async Task<IActionResult> Post(string linkName, [FromBody]TagViewModel vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newTag = Mapper.Map<Tags>(vm);

                    //_repository.AddTag(linkName, newTag, User.Identity.Name);
                    if (await _repository.SaveChangesAsync())
                    {
                        return Created($"/api/links/{linkName}/tags/{newTag.Name}",
                        Mapper.Map<TagViewModel>(newTag));
                    }                   
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed to save new tag: {0}", ex);
            }
            return BadRequest("Failed to save new tag");
        }
    }
}
