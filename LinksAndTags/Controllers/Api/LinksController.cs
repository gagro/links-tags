﻿using AutoMapper;
using LinksAndTags.Models;
using LinksAndTags.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinksAndTags.Controllers.Api
{
    [Route("api/links")]
    [Authorize]
    public class LinksController : Controller
    {
        private ILinksAndTagsRepository _repository;
        private ILogger<LinksController> _logger;
        private LinksAndTagsContext _context;

        public LinksController(ILinksAndTagsRepository repository, ILogger<LinksController> logger, LinksAndTagsContext context)
        {
            _repository = repository;
            _logger = logger;
            _context = context;
        }

        [HttpGet("{name}")]
        public IActionResult GetByName(string name)
        {
            try
            {
                var result = _repository.GetUserLinkByName(name, User.Identity.Name);
                return Ok(Mapper.Map<LinkViewModel>(result));
            }
            catch (Exception ex)
            {

                _logger.LogError($"Failed to load link: {ex}");
                return BadRequest($"Error occured: {ex}");
            }           
        }

        [HttpGet("")]
        public IActionResult Get()
        {
            try
            {
                var results = _repository.GetLinksByUsername(User.Identity.Name);
                return Ok(Mapper.Map<IEnumerable<LinkViewModel>>(results));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get all links: {ex}");
                return BadRequest("Error occured");
            }           
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody]FinalFormViewModel theLink)
        {
            if (ModelState.IsValid)
            {
                var newLink = new Links
                {
                    UserName = User.Identity.Name,
                    Url = theLink.Url,
                    Name = theLink.Name,
                    DateCreated = DateTime.Now
                };
                //newLink.Name = _repository.GetTitle(newLink.Url);

                _repository.AddLink(newLink);
                _context.SaveChanges();
                Links link = _repository.GetLinkByName(newLink.Name);

                foreach (var tag in theLink.Tags)
                {
                    var newTag = new Tags
                    {
                        Name = tag
                    };
                    _repository.AddTag(link, newTag, newLink.UserName);
                }

                // Returning data

                var data = new FinalFormViewModel()
                {
                    Name = newLink.Name,
                    Url = newLink.Url,
                    DateCreated = newLink.DateCreated,
                    Tags = newLink.Tags.Select(t => t.Name).ToList()
                };
                if (await _repository.SaveChangesAsync())
                {
                    return Created($"api/links/{theLink.Url}", data);
                }             
            }
            return BadRequest("Failed to save the link");
        }

        [HttpPut("{name}")]
        public async Task<IActionResult> Update(string name, [FromBody] FinalFormViewModel theLink)
        {
            if (ModelState.IsValid)
            {
                var updatedLink = _context.Links.Include(t => t.Tags).FirstOrDefault(l => l.Name == name);
                if (updatedLink == null)
                {
                    return NotFound();
                }

                updatedLink.Name = theLink.Name;
                updatedLink.DateCreated = DateTime.UtcNow;
                _context.Links.Update(updatedLink);


                //TO DO
                //ICollection<Tags> updatedTags = new List<Tags>();
                //foreach (var tag in theLink.Tags)
                //{
                //    var newTag = new Tags
                //    {
                //        Name = tag
                //    };
                //    _repository.AddTag(updatedLink, newTag, updatedLink.UserName);
                //}

                if (await _repository.SaveChangesAsync())
                {
                    return new NoContentResult();
                }
            }
            return BadRequest();
        }

        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete([FromRoute]string name)
        {
            if (ModelState.IsValid)
            {
                var linkToRemove = _repository.GetUserLinkByName(name, User.Identity.Name);
                if (linkToRemove.Name == null)
                {
                    return NotFound();
                }
                
                _context.Links.Remove(linkToRemove);
                _context.Tags.RemoveRange(linkToRemove.Tags);

                if (await _repository.SaveChangesAsync())
                {
                    return Ok();
                }
            }
            return BadRequest(ModelState);
        }
    }
}
