﻿using LinksAndTags.Models;
using LinksAndTags.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace LinksAndTags.Controllers.Api
{
    //[Authorize]
    [Route("api/finalform")]
    public class FinalFormController : Controller
    {
        private ILinksAndTagsRepository _repository;
        private ILogger<LinksController> _logger;

        public FinalFormController(ILinksAndTagsRepository repository, ILogger<LinksController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("")]
        public IActionResult Get([FromQuery(Name = "url")]string url)
        {
            try
            {
                var html = _repository.GetHtml(url).Result;
                var title = html.Title;
                var body = _repository.GetBody(html);
                var punctuation = _repository.RemovePunctuation(body);
                var removeStopwords = _repository.RemoveStopwords(punctuation);
                var textList = removeStopwords.Split(' ').ToList();
                var tags = _repository.GetTags(html, textList);
                var result = new FinalFormViewModel() { Name = title, Url= url, Tags = tags};

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get data: {ex}");
                return BadRequest("Error occured");
            }
        }
    }
}
