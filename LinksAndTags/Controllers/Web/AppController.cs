﻿using Microsoft.AspNetCore.Mvc;
using LinksAndTags.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinksAndTags.Services;
using Microsoft.Extensions.Configuration;
using LinksAndTags.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace LinksAndTags.Controllers.Web
{
    public class AppController : Controller
    {
        private IMailService _mailService;
        private IConfigurationRoot _config;

        public AppController(IMailService mailService, 
            IConfigurationRoot config, 
            LinksAndTagsContext context, 
            ILinksAndTagsRepository repository,
            ILogger<AppController> logger)
        {
            _mailService = mailService;
            _config = config;
        }

        public IActionResult Index()
        {
            return View();                       
        }

        [Authorize]
        public IActionResult Links()
        {
            return View();
        }

        public IActionResult About()
        {           
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Contact(ContactViewModel model)
        {
            _mailService.SendMail(_config["MailSettings:ToAddress"], model.Email, "From Links", model.Message);
            ModelState.Clear();
            ViewBag.UserMessage = "Message Sent";
            return View();
        }
    }
}
