﻿import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Tag } from "../shared/tag";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { DataService } from "../shared/dataService";
import { Link } from "../shared/link";

@Injectable()
export class EditorResolver implements Resolve<Link> {

    constructor(private service: DataService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Link> {
        let name = encodeURIComponent(route.params['linkName']);
        return this.service.loadLinkByName(name);
    }
}