import { Component, OnInit, ViewChild } from '@angular/core';
//import { ViewChild } from '@angular/core/src/metadata';
import { NgForm } from '@angular/forms';
import { Link } from '../shared/link';
import { ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: "link-edit-info.component.html"
})
export class LinkEditInfoComponent implements OnInit {
    @ViewChild(NgForm) linkForm: NgForm;
    public link: Link;

    constructor(private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.route.parent.data.subscribe(data => {
            this.link = data['link'];

            if (this.linkForm) {
                this.linkForm.reset();
            }
        })
    }

}
