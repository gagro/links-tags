﻿import { Component, OnInit } from "@angular/core";
import { DataService } from '../shared/dataService';
import { Tag } from "../shared/tag";
import { ActivatedRoute } from '@angular/router';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Link } from "../shared/link";
import { Router } from "@angular/router";

@Component({
    selector: "editor",
    templateUrl: "editor.component.html",
    styleUrls: ['editor.component.css']
})

export class EditorComponent implements OnInit {
    constructor(public data: DataService, private router: Router, private route: ActivatedRoute, private http: HttpClient) {

    }
    public link: Link;
    public errorMessage = "";
    public isBusy = true;
    public title: string;

    private updateLink() {
        this.data.updateLink(this.title, this.link)
            .subscribe(data =>
                this.router.navigate(["/"]));
    }

    private deleteLink() {
        this.data.deleteLink(encodeURIComponent(this.link.name))
            .subscribe(data =>
                this.router.navigate(["/"]));
    };


    ngOnInit(): void {
        //let name = this.route.snapshot.paramMap.get('linkName');
        //this.data.loadTags(name)
        //    .subscribe(tags => {
        //        this.tags = tags;
        //    });
        this.title = this.route.snapshot.params['linkName'];
        this.link = this.route.snapshot.data['link'];
    }
}