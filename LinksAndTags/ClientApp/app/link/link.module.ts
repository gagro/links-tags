import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinkListComponent } from "../link/linkList.component";
import { LinkComponent } from "./link.component";
import { EditorComponent } from "../editor/editor.component";
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DataService } from "../shared/dataService";
import { FinalFormComponent } from '../finalForm/finalForm.comptonent';
import { FinalFormResolver } from '../finalForm/finalForm-resolver.service';
import { EditorResolver } from '../editor/editor-resolver.service';
import { LinkEditInfoComponent } from '../editor/link-edit-info.component';
import { LinkEditTagsComponent } from '../editor/link-edit-tags.component';
import { TagComponent } from '../tag/tag.component';
import { TagResolver } from '../tag/tag-resolver.service';
import { LinkListResolver } from './linkList-resolver.service';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      RouterModule.forChild([ 
          { path: '', component: LinkComponent, resolve: { linkList : LinkListResolver}, pathMatch: 'full' },
          {
              path: ":linkName/edit",
              component: EditorComponent,
              resolve: { link: EditorResolver },
              children: [
                  {
                      path: '', redirectTo: 'info', pathMatch: 'full'
                  },
                  {
                      path: 'info', component: LinkEditInfoComponent
                  },
                  {
                      path: 'tags', component: LinkEditTagsComponent
                  }
              ]
          },
          { path: 'finalForm', component: FinalFormComponent, resolve: { finalData: FinalFormResolver } },
          { path: ':linkName/tags', component: TagComponent, resolve: { tags: TagResolver } }
      ])
  ],
  declarations: [
      LinkListComponent,
      LinkComponent,
      EditorComponent,
      LinkEditInfoComponent,
      LinkEditTagsComponent
  ],
  providers: [
      FinalFormResolver,
      EditorResolver,
      TagResolver,
      LinkListResolver
  ]
})
export class LinkModule { }
