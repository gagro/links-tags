﻿import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { DataService } from "../shared/dataService";
import { Link } from "../shared/link";

@Injectable()
export class LinkListResolver implements Resolve<Link[]> {

    constructor(private service: DataService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Link[]> {
        return this.service.loadLinks();
    }
}