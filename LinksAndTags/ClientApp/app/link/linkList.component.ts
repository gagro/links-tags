﻿import { Component, OnInit } from "@angular/core";
import { DataService } from "../shared/dataService";
import { Link } from "../shared/link";
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    selector: "link-list",
    templateUrl: "linkList.component.html",
    styleUrls: []
})

export class LinkListComponent implements OnInit {

    constructor(private data: DataService, private route: ActivatedRoute, private router: Router) {
    }
    public links: Link[];
    public filteredLinks: Link[];
   
    _listFilter: string;
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(value: string) {
        this._listFilter = value;
        this.filteredLinks = this.listFilter ? this.performFilter(this.listFilter) : this.links;
    }
    
    public errorMessage: string;
    public isBusy = true;
    public url: string;

    performFilter(filterBy: string): Link[] {
        filterBy = filterBy.toLocaleLowerCase();
        return this.links.filter((link: Link) =>
            link.name.toLocaleLowerCase().indexOf(filterBy) !== -1);
    }

    private deleteLink(name: string) {
        this.data.deleteLink(encodeURIComponent(name))
            .subscribe(data => {
                this.ngOnInit();
    }
    )};

    ngOnInit(): void {        
        this.data.loadLinks()
            .subscribe(links => {
                this.links = links;                
                this.filteredLinks = this.links
                this.filteredLinks = this.filteredLinks.reverse();
                this.listFilter = this.route.snapshot.queryParams['filterBy'] || '';
                this.isBusy = false;
            },
            error => this.errorMessage = <any>error);

        // TO DO this section 
        this.route.data.map(data => data.linkList).subscribe(links => {
            this.links = links;
            this.filteredLinks = this.links
            this.filteredLinks = this.filteredLinks.reverse();
            this.listFilter = this.route.snapshot.queryParams['filterBy'] || '';
            this.isBusy = false;
        });
    }
}