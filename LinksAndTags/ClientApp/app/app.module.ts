import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { DataService } from "./shared/dataService";
import { FinalFormComponent} from "./finalForm/finalForm.comptonent";
import { LinkModule } from './link/link.module';
import { LinkComponent } from './link/link.component';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './page-not-found.component';
import { LinkEditInfoComponent } from './editor/link-edit-info.component';
import { LinkEditTagsComponent } from './editor/link-edit-tags.component';
import { FormsModule } from '@angular/forms';
import { TagComponent } from './tag/tag.component';


@NgModule({
  declarations: [
      AppComponent,
      FinalFormComponent,
      PageNotFoundComponent,
      TagComponent
  ],
  imports: [
      BrowserModule,
      HttpClientModule,
      LinkModule,
      AppRoutingModule,
      FormsModule
  ],
  providers: [
      DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
