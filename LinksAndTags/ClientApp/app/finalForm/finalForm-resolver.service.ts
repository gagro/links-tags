﻿import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { FinalFormData, } from "../shared/finalFormData";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { DataService } from "../shared/dataService";

@Injectable()
export class FinalFormResolver implements Resolve<FinalFormData> {

    constructor(private service: DataService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FinalFormData>{
        let url: string = route.queryParams['url'];
        if (!url.startsWith('http://') && (!url.startsWith('https://'))) {
            url = 'https://' + url;
        }
        let encodedUrl = encodeURIComponent(url);
        return this.service.loadData(encodedUrl);
    }
}
