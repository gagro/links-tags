﻿import { Component, OnInit, Injectable } from "@angular/core";
import { DataService } from "../shared/dataService";
import { FinalFormData } from "../shared/finalFormData";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from "rxjs/Observable";
import { NgForm } from "@angular/forms";

@Component({
    selector: "final-form",
    templateUrl: "finalForm.component.html"
})

export class FinalFormComponent implements OnInit{

    constructor(private data: DataService, private route: ActivatedRoute, private router: Router) {
    }
    public finalData: FinalFormData;
    public errorMessage = "";
    public isBusy = true;
    public checkboxTag: string[] = [];
    newTags: string;

    CheckBoxValueChange(isChecked: boolean, checkboxValue: string) {
        if (isChecked) {
            this.checkboxTag.push(checkboxValue);
        }
        else {
            this.checkboxTag.splice(this.checkboxTag.indexOf(checkboxValue), 1);
        }
    }

    submitForm() {
        this.finalData.tags = this.checkboxTag;
        if (this.newTags != undefined) {
            let inputTags = this.newTags.split(',');
            for (let i of inputTags) {
                i = i.trim();
            }
            this.finalData.tags = this.finalData.tags.concat(inputTags);
        }
        
        this.data.postData(this.finalData)
            .subscribe(data => {
                this.finalData = data;
                this.router.navigate(["/"]);
            }, err => this.errorMessage = err);
    }

    ngOnInit(): void {
        //let url = encodeURIComponent(this.route.snapshot.queryParams['url']);
        //this.data.loadData(url)
        //    .subscribe(finalData => {
        //        this.finalData = finalData;
        //    }); 
        this.finalData = this.route.snapshot.data['finalData'];
    }
}