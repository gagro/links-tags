import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/dataService';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Tag } from '../shared/tag';

@Component({
  selector: 'app-tag',
  templateUrl: "tag.component.html"
})
export class TagComponent implements OnInit {

    constructor(public data: DataService, private route: ActivatedRoute, private http: HttpClient) {

    }
    public tags: Tag[] = [];
    public title: string;

    ngOnInit(): void {
        //let name = this.route.snapshot.paramMap.get('linkName');
        //this.data.loadTags(name)
        //    .subscribe(tags => {
        //        this.tags = tags;
        //    });
        this.title = this.route.snapshot.params['linkName'];
        this.tags = this.route.snapshot.data['tags'];
    }

}
