﻿import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Tag } from "../shared/tag";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { DataService } from "../shared/dataService";

@Injectable()
export class TagResolver implements Resolve<Tag[]> {

    constructor(private service: DataService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Tag[]> {
        let name = encodeURIComponent(route.params['linkName']);
        return this.service.loadTags(name);
    }
}