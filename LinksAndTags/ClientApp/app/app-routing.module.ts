import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LinkComponent } from './link/link.component';
import { PageNotFoundComponent } from './page-not-found.component';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forRoot([
          { path: '**', component: PageNotFoundComponent }],          
          { useHash: true })
    ],
    exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
