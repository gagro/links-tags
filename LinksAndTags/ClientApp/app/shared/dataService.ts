﻿import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { Link } from "./link";
import { Tag } from "./tag"
import { Injectable, OnInit } from "@angular/core";

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { ActivatedRoute } from '@angular/router';
import { FinalFormData} from "./finalFormData";
import { catchError } from "rxjs/operators/catchError";


@Injectable()
export class DataService {
    constructor(private http: HttpClient, private route: ActivatedRoute) { }

    private finalData: FinalFormData;
    private header: Headers;

    // Load links on landing page
    public loadLinks(): Observable<Link[]> {
        return this.http.get<Link[]>("/api/links")
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: HttpErrorResponse) {
        console.log(error.message);
        return Observable.throw(error.message);
    }

    // Load link by name and username

    public loadLinkByName(name: string): Observable<Link> {
        return this.http.get<Link>(`/api/links/${name}`);
    }

    // Load link name, link url and tags from API

    public loadData(url: string): Observable<FinalFormData> {
        return this.http.get<FinalFormData>("/api/finalform?url=" + url);
    }

    // Load tags of given link

    public loadTags(name: string): Observable<Tag[]> {
        return this.http.get<Tag[]>("/api/links/" + name + "/tags");
    }

    // Post data to server

    public postData(data: FinalFormData): Observable<FinalFormData> {
        //console.log('data: ', data);
        //let header = new HttpHeaders();
        //header.append('Content-Type', 'application/json;charset=utf-8')

        return this.http.post<FinalFormData>("/api/links", data, {
            headers: new HttpHeaders({
                'Content-Type' : 'application/json; charset=utf-8'
            })
        });


    }

    // Update link 

    public updateLink(name: string, data: FinalFormData): Observable<FinalFormData> {
        return this.http.put<FinalFormData>(`/api/links/${name}`, data, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    }

    // Delete link

    public deleteLink(name: string): Observable<FinalFormData> {
        return this.http.delete<FinalFormData>(`/api/links/${name}`);
    }
}