﻿export class FinalFormData {
    name: string;
    url: string;
    dateCreated: Date;
    tags: any[];
}