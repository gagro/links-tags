﻿export interface Link {
    name: string;
    url: string;
    dateCreated: Date;
    tags: string[];
}