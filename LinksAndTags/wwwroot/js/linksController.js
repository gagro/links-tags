﻿(function () {

    "use strict";

    angular.module("app-links")
        .controller("linksController", linksController);
    function linksController($http) {
        var vm = this;
        vm.links = [];

        vm.newLink = {};

        vm.errorMessage = "";

        vm.isBusy = true;

        $http.get("/api/links")
            .then(function (response) {
                // success
                angular.copy(response.data, vm.links);
            }, function (error) {
                // FAILURE
                vm.errorMessage = "Failed to load data: " + error;
            })
            .finally(function () {
                vm.isBusy = false;
            });
        vm.addLink = function () {
            vm.isBusy = true;
            vm.errorMessage = "";

            $http.post("/api/links", vm.newLink)
                .then(function (response) {
                    // success
                    vm.links.push(response.data);
                    vm.newLink = {};
                }, function () {
                    // FAILURE
                    vm.errorMessage = "Failed to save new link";
                })
                .finally(function () {
                    vm.isBusy = false;
                })
        }
    }
})();