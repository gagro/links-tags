﻿(function () {
    "use strict";

    angular.module("app-links", ["ngRoute"])
        .config(function ($routeProvider) {
            $routeProvider.when("/", {
                controller: "linksController",
                controllerAs: "vm",
                templateUrl: "/views/linksView.html"
            });

            $routeProvider.when("/editor/:linkName", {
                controller: "linkEditorController",
                controllerAs: "vm",
                templateUrl: "/views/linkEditorView.html"
            });

            $routeProvider.otherwise({ redirectTo: "/" });
        })
        .config(['$locationProvider',
            function ($locationProvider) {
                $locationProvider.hashPrefix('');
            }]);
}) ();