﻿(function () {
    "use strict";

    angular.module("app-links")
        .controller("linkEditorController", linkEditorController);
    function linkEditorController($routeParams, $http) {
        var vm = this;

        vm.linkName = $routeParams.linkName;
        vm.tags = [];
        vm.errorMessage = "";
        vm.isBusy = true;
        vm.newTag = {};

        $http.get("/api/links/" + vm.linkName + "/tags")
            .then(function (response) {
                //succes
                angular.copy(response.data, vm.tags);
            }, function (err) {
                //failure
                vm.errorMessage = "Failed to load stops";
            })
            .finally(function () {
                vm.isBusy = false;
            });

        vm.addTag = function () {
            vm.isBusy = true;

            $http.post("/api/links/" + vm.linkName + "/tags", vm.newTag)
                .then(function (response) {
                    //success
                    vm.tags.push(response.data);
                    vm.newTag = {};
                }, function (err) {
                    //failure
                    vm.errorMessage = "Failed to add new tag";
                })
                .finally(function () {
                    vm.isBusy = false;
                });
        }
    }
})();