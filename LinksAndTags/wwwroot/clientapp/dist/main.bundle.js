webpackJsonp(["main"],{

/***/ "./ClientApp/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./ClientApp/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./ClientApp/app/app-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var page_not_found_component_1 = __webpack_require__("./ClientApp/app/page-not-found.component.ts");
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forRoot([
                    { path: '**', component: page_not_found_component_1.PageNotFoundComponent }
                ], { useHash: true })
            ],
            exports: [router_1.RouterModule],
            declarations: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ }),

/***/ "./ClientApp/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<span *ngIf=\"loading\" class=\"glyphicon glyphicon-refresh glyphicon-spin spinner\"></span>\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./ClientApp/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        var _this = this;
        this.router = router;
        this.loading = true;
        this.title = 'Links';
        router.events.subscribe(function (routerEvent) {
            _this.checkRouterEvent(routerEvent);
        });
    }
    AppComponent.prototype.checkRouterEvent = function (routerEvent) {
        if (routerEvent instanceof router_1.NavigationStart) {
            this.loading = true;
        }
        if (routerEvent instanceof router_1.NavigationEnd ||
            routerEvent instanceof router_1.NavigationCancel ||
            routerEvent instanceof router_1.NavigationError) {
            this.loading = false;
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'the-link',
            template: __webpack_require__("./ClientApp/app/app.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "./ClientApp/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var app_component_1 = __webpack_require__("./ClientApp/app/app.component.ts");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var finalForm_comptonent_1 = __webpack_require__("./ClientApp/app/finalForm/finalForm.comptonent.ts");
var link_module_1 = __webpack_require__("./ClientApp/app/link/link.module.ts");
var app_routing_module_1 = __webpack_require__("./ClientApp/app/app-routing.module.ts");
var page_not_found_component_1 = __webpack_require__("./ClientApp/app/page-not-found.component.ts");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var tag_component_1 = __webpack_require__("./ClientApp/app/tag/tag.component.ts");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                finalForm_comptonent_1.FinalFormComponent,
                page_not_found_component_1.PageNotFoundComponent,
                tag_component_1.TagComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                link_module_1.LinkModule,
                app_routing_module_1.AppRoutingModule,
                forms_1.FormsModule
            ],
            providers: [
                dataService_1.DataService
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "./ClientApp/app/editor/editor-resolver.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var EditorResolver = /** @class */ (function () {
    function EditorResolver(service) {
        this.service = service;
    }
    EditorResolver.prototype.resolve = function (route, state) {
        var name = encodeURIComponent(route.params['linkName']);
        return this.service.loadLinkByName(name);
    };
    EditorResolver = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [dataService_1.DataService])
    ], EditorResolver);
    return EditorResolver;
}());
exports.EditorResolver = EditorResolver;


/***/ }),

/***/ "./ClientApp/app/editor/editor.component.css":
/***/ (function(module, exports) {

module.exports = ".glyphicon-exclamation-sign {\r\n    color: red;\r\n}\r\n\r\n.wizard a {\r\n    background: #efefef;\r\n    display: inline-block;\r\n    margin-right: 5px;\r\n    min-width: 150px;\r\n    outline: none;\r\n    padding: 10px 40px 10px;\r\n    position: relative;\r\n    text-decoration: none;\r\n}\r\n\r\n.wizard a:hover {\r\n        cursor: pointer;\r\n        text-decoration: underline;\r\n    }\r\n\r\n/* Adds the cut out on the left side of the tab */\r\n\r\n.wizard a:before {\r\n        width: 0;\r\n        height: 0;\r\n        border-top: 20px inset transparent;\r\n        border-bottom: 20px inset transparent;\r\n        border-left: 20px solid #fff;\r\n        position: absolute;\r\n        content: \"\";\r\n        top: 0;\r\n        left: 0;\r\n    }\r\n\r\n/* Adds the arrow on the right side of the tab */\r\n\r\n.wizard a:after {\r\n        width: 0;\r\n        height: 0;\r\n        border-top: 20px inset transparent;\r\n        border-bottom: 20px inset transparent;\r\n        border-left: 21px solid #efefef;\r\n        position: absolute;\r\n        content: \"\";\r\n        top: 0;\r\n        right: -20px;\r\n        z-index: 2;\r\n    }\r\n\r\n/* Squares the start and end of the tab bar */\r\n\r\n.wizard a:first-child:before,\r\n    .wizard a:last-child:after {\r\n        border: none;\r\n    }\r\n\r\n/* Rounds the corners */\r\n\r\n.wizard a:first-child {\r\n        border-radius: 8px 0 0 0px;\r\n    }\r\n\r\n.wizard a:last-child {\r\n        border-radius: 0 8px 0px 0;\r\n    }\r\n\r\n.wizard .active {\r\n    background: #007ACC;\r\n    color: #fff;\r\n}\r\n\r\n/* Adds the right arrow after the tab */\r\n\r\n.wizard .active:after {\r\n        border-left-color: #007ACC;\r\n    }\r\n"

/***/ }),

/***/ "./ClientApp/app/editor/editor.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <div><a [routerLink]=\"['/']\" queryParamsHandling=\"preserve\" class=\"btn btn-group-sm btn-default\"><i class=\"fa fa-angle-left\"></i> Back</a></div>\r\n</div>\r\n<div class=\"panel panel-primary\">\r\n    <div class=\"panel-heading\">\r\n        {{ title }}\r\n    </div>\r\n\r\n<div class=\"panel-body\" *ngIf=\"link\">\r\n    <div class=\"wizard\">\r\n        <a [routerLink]=\"['info']\" routerLinkActive=\"active\">\r\n            Basic Information\r\n        </a>\r\n        <a [routerLink]=\"['tags']\" routerLinkActive=\"active\">\r\n            Edit tags\r\n        </a>\r\n    </div>\r\n\r\n    <router-outlet></router-outlet>\r\n</div>\r\n\r\n<div class=\"panel-footer\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-6 col-md-offset-2\">\r\n            <span>\r\n                <button class=\"btn btn-primary\" type=\"button\" (click)=\"updateLink()\">\r\n                    Update\r\n                </button>\r\n            </span>\r\n            <span>\r\n                <a class=\"btn btn-default\" [routerLink]=\"['/']\">\r\n                    Cancel\r\n                </a>\r\n            </span>\r\n            <span>\r\n                <a class=\"btn btn-default\"\r\n                   (click)=\"deleteLink()\">\r\n                    Delete\r\n                </a>\r\n            </span>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"has-error\" *ngIf=\"errorMessage\">{{errorMessage}}</div>\r\n</div>\r\n"

/***/ }),

/***/ "./ClientApp/app/editor/editor.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var router_2 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var EditorComponent = /** @class */ (function () {
    function EditorComponent(data, router, route, http) {
        this.data = data;
        this.router = router;
        this.route = route;
        this.http = http;
        this.errorMessage = "";
        this.isBusy = true;
    }
    EditorComponent.prototype.updateLink = function () {
        var _this = this;
        this.data.updateLink(this.title, this.link)
            .subscribe(function (data) {
            return _this.router.navigate(["/"]);
        });
    };
    EditorComponent.prototype.deleteLink = function () {
        var _this = this;
        this.data.deleteLink(encodeURIComponent(this.link.name))
            .subscribe(function (data) {
            return _this.router.navigate(["/"]);
        });
    };
    ;
    EditorComponent.prototype.ngOnInit = function () {
        //let name = this.route.snapshot.paramMap.get('linkName');
        //this.data.loadTags(name)
        //    .subscribe(tags => {
        //        this.tags = tags;
        //    });
        this.title = this.route.snapshot.params['linkName'];
        this.link = this.route.snapshot.data['link'];
    };
    EditorComponent = __decorate([
        core_1.Component({
            selector: "editor",
            template: __webpack_require__("./ClientApp/app/editor/editor.component.html"),
            styles: [__webpack_require__("./ClientApp/app/editor/editor.component.css")]
        }),
        __metadata("design:paramtypes", [dataService_1.DataService, router_2.Router, router_1.ActivatedRoute, http_1.HttpClient])
    ], EditorComponent);
    return EditorComponent;
}());
exports.EditorComponent = EditorComponent;


/***/ }),

/***/ "./ClientApp/app/editor/link-edit-info.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"panel-body\">\r\n    <form class=\"form-horizontal\"\r\n          novalidate\r\n          #linkForm=\"ngForm\">\r\n        <fieldset>\r\n            <legend>Link information</legend>\r\n            <div class=\"form-group\">\r\n                <label class=\"col-md-2 control-label\"\r\n                       for=\"linkName\">Link name</label>\r\n\r\n                <div class=\"col-md-8\">\r\n                    <input class=\"form-control\" id=\"linkName\" type=\"text\" \r\n                           placeholder=\"Name (required)\" required minlength=\"3\" \r\n                           [(ngModel)]=\"link.name\" name=\"linkName\"/>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n                <label class=\"col-md-2 control-label\" for=\"urlInput\">Url</label>\r\n\r\n                <div class=\"col-md-8\">\r\n                    <input class=\"form-control\" id=\"urlInput\" type=\"text\" \r\n                           placeholder=\"URL (required)\" required [(ngModel)]=link.url \r\n                           name=\"url\" readonly />\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"has-error\" *ngIf=\"errorMessage\">{{errorMessage}}</div>\r\n        </fieldset>\r\n    </form>\r\n</div>"

/***/ }),

/***/ "./ClientApp/app/editor/link-edit-info.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
//import { ViewChild } from '@angular/core/src/metadata';
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var LinkEditInfoComponent = /** @class */ (function () {
    function LinkEditInfoComponent(route) {
        this.route = route;
    }
    LinkEditInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.parent.data.subscribe(function (data) {
            _this.link = data['link'];
            if (_this.linkForm) {
                _this.linkForm.reset();
            }
        });
    };
    __decorate([
        core_1.ViewChild(forms_1.NgForm),
        __metadata("design:type", forms_1.NgForm)
    ], LinkEditInfoComponent.prototype, "linkForm", void 0);
    LinkEditInfoComponent = __decorate([
        core_1.Component({
            template: __webpack_require__("./ClientApp/app/editor/link-edit-info.component.html")
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute])
    ], LinkEditInfoComponent);
    return LinkEditInfoComponent;
}());
exports.LinkEditInfoComponent = LinkEditInfoComponent;


/***/ }),

/***/ "./ClientApp/app/editor/link-edit-tags.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var LinkEditTagsComponent = /** @class */ (function () {
    function LinkEditTagsComponent() {
    }
    LinkEditTagsComponent.prototype.ngOnInit = function () {
    };
    LinkEditTagsComponent = __decorate([
        core_1.Component({
            selector: 'app-link-edit-tags',
            template: "\n    <p>\n      link-edit-tags works!\n    </p>\n  ",
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], LinkEditTagsComponent);
    return LinkEditTagsComponent;
}());
exports.LinkEditTagsComponent = LinkEditTagsComponent;


/***/ }),

/***/ "./ClientApp/app/finalForm/finalForm-resolver.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var FinalFormResolver = /** @class */ (function () {
    function FinalFormResolver(service) {
        this.service = service;
    }
    FinalFormResolver.prototype.resolve = function (route, state) {
        var url = route.queryParams['url'];
        if (!url.startsWith('http://') && (!url.startsWith('https://'))) {
            url = 'https://' + url;
        }
        var encodedUrl = encodeURIComponent(url);
        return this.service.loadData(encodedUrl);
    };
    FinalFormResolver = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [dataService_1.DataService])
    ], FinalFormResolver);
    return FinalFormResolver;
}());
exports.FinalFormResolver = FinalFormResolver;


/***/ }),

/***/ "./ClientApp/app/finalForm/finalForm.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-6 col-md-offset-3\">\r\n    <div><a [routerLink]=\"'/'\" class=\"btn btn-group-sm btn-default\"><i class=\"fa fa-angle-left\"></i> Back</a></div>\r\n    <h3>Data</h3>\r\n    <div class=\"alert alert-warning\" *ngIf=\"errorMessage\">{{ errorMessage }}</div>\r\n    <form #form=\"ngForm\" (ngSubmit)=\"submitForm()\" novalidate>\r\n        <div class=\"form-group\" [class.has-error]=\"linkName.invalid\">\r\n            <label class=\"control-label\" for=\"name\">Name</label>\r\n            <input #linkName=\"ngModel\" type=\"text\" minlength=\"5\" class=\"form-control\" name=\"name\" id=\"name\" [(ngModel)]=\"finalData.name\" required />\r\n            <div *ngIf=\"linkName.invalid\" class=\"alert alert-danger\">Name is required</div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"url\">Url</label>\r\n            <input type=\"text\" class=\"form-control\" id=\"url\" name=\"url\" [(ngModel)]=\"finalData.url\" readonly/>\r\n        </div>\r\n        <h4>Would you like to add some of these tags?</h4>\r\n        <div class=\"checkbox\" *ngFor=\"let tag of finalData.tags; index as i\" [attr.data-index]=\"i\">\r\n            <label><input #checkTag type=\"checkbox\" name=\"{{ 'checkbox' + tag }}\" id=\"{{ 'checkbox' + tag }}\" (ngModel)=\"finalData.tags[i]\" (ngModelChange)=\"CheckBoxValueChange(checkTag.checked, finalData.tags[i])\"/>{{ tag }}</label>\r\n        </div> \r\n        <div class=\"form-group\">\r\n            <input type=\"text\" class=\"form-control\" name=\"inputTags\" [(ngModel)]=\"newTags\" placeholder=\"Add tags separated by commas\" />\r\n        </div>\r\n        <button type=\"submit\" [disabled]=\"form.invalid\" class=\"btn btn-primary btn-success\">Save</button>\r\n    </form>\r\n    Angular: {{ form.value | json }}<br />\r\n    Model: {{ finalData | json }}\r\n</div>"

/***/ }),

/***/ "./ClientApp/app/finalForm/finalForm.comptonent.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var FinalFormComponent = /** @class */ (function () {
    function FinalFormComponent(data, route, router) {
        this.data = data;
        this.route = route;
        this.router = router;
        this.errorMessage = "";
        this.isBusy = true;
        this.checkboxTag = [];
    }
    FinalFormComponent.prototype.CheckBoxValueChange = function (isChecked, checkboxValue) {
        if (isChecked) {
            this.checkboxTag.push(checkboxValue);
        }
        else {
            this.checkboxTag.splice(this.checkboxTag.indexOf(checkboxValue), 1);
        }
    };
    FinalFormComponent.prototype.submitForm = function () {
        var _this = this;
        this.finalData.tags = this.checkboxTag;
        if (this.newTags != undefined) {
            var inputTags = this.newTags.split(',');
            for (var _i = 0, inputTags_1 = inputTags; _i < inputTags_1.length; _i++) {
                var i = inputTags_1[_i];
                i = i.trim();
            }
            this.finalData.tags = this.finalData.tags.concat(inputTags);
        }
        this.data.postData(this.finalData)
            .subscribe(function (data) {
            _this.finalData = data;
            _this.router.navigate(["/"]);
        }, function (err) { return _this.errorMessage = err; });
    };
    FinalFormComponent.prototype.ngOnInit = function () {
        //let url = encodeURIComponent(this.route.snapshot.queryParams['url']);
        //this.data.loadData(url)
        //    .subscribe(finalData => {
        //        this.finalData = finalData;
        //    }); 
        this.finalData = this.route.snapshot.data['finalData'];
    };
    FinalFormComponent = __decorate([
        core_1.Component({
            selector: "final-form",
            template: __webpack_require__("./ClientApp/app/finalForm/finalForm.component.html")
        }),
        __metadata("design:paramtypes", [dataService_1.DataService, router_1.ActivatedRoute, router_1.Router])
    ], FinalFormComponent);
    return FinalFormComponent;
}());
exports.FinalFormComponent = FinalFormComponent;


/***/ }),

/***/ "./ClientApp/app/link/link.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n        <link-list></link-list>        \r\n    </div>\r\n</div>"

/***/ }),

/***/ "./ClientApp/app/link/link.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var LinkComponent = /** @class */ (function () {
    function LinkComponent() {
    }
    LinkComponent = __decorate([
        core_1.Component({
            template: __webpack_require__("./ClientApp/app/link/link.component.html")
        })
    ], LinkComponent);
    return LinkComponent;
}());
exports.LinkComponent = LinkComponent;


/***/ }),

/***/ "./ClientApp/app/link/link.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var linkList_component_1 = __webpack_require__("./ClientApp/app/link/linkList.component.ts");
var link_component_1 = __webpack_require__("./ClientApp/app/link/link.component.ts");
var editor_component_1 = __webpack_require__("./ClientApp/app/editor/editor.component.ts");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var finalForm_comptonent_1 = __webpack_require__("./ClientApp/app/finalForm/finalForm.comptonent.ts");
var finalForm_resolver_service_1 = __webpack_require__("./ClientApp/app/finalForm/finalForm-resolver.service.ts");
var editor_resolver_service_1 = __webpack_require__("./ClientApp/app/editor/editor-resolver.service.ts");
var link_edit_info_component_1 = __webpack_require__("./ClientApp/app/editor/link-edit-info.component.ts");
var link_edit_tags_component_1 = __webpack_require__("./ClientApp/app/editor/link-edit-tags.component.ts");
var tag_component_1 = __webpack_require__("./ClientApp/app/tag/tag.component.ts");
var tag_resolver_service_1 = __webpack_require__("./ClientApp/app/tag/tag-resolver.service.ts");
var linkList_resolver_service_1 = __webpack_require__("./ClientApp/app/link/linkList-resolver.service.ts");
var LinkModule = /** @class */ (function () {
    function LinkModule() {
    }
    LinkModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                router_1.RouterModule.forChild([
                    { path: '', component: link_component_1.LinkComponent, resolve: { linkList: linkList_resolver_service_1.LinkListResolver }, pathMatch: 'full' },
                    {
                        path: ":linkName/edit",
                        component: editor_component_1.EditorComponent,
                        resolve: { link: editor_resolver_service_1.EditorResolver },
                        children: [
                            {
                                path: '', redirectTo: 'info', pathMatch: 'full'
                            },
                            {
                                path: 'info', component: link_edit_info_component_1.LinkEditInfoComponent
                            },
                            {
                                path: 'tags', component: link_edit_tags_component_1.LinkEditTagsComponent
                            }
                        ]
                    },
                    { path: 'finalForm', component: finalForm_comptonent_1.FinalFormComponent, resolve: { finalData: finalForm_resolver_service_1.FinalFormResolver } },
                    { path: ':linkName/tags', component: tag_component_1.TagComponent, resolve: { tags: tag_resolver_service_1.TagResolver } }
                ])
            ],
            declarations: [
                linkList_component_1.LinkListComponent,
                link_component_1.LinkComponent,
                editor_component_1.EditorComponent,
                link_edit_info_component_1.LinkEditInfoComponent,
                link_edit_tags_component_1.LinkEditTagsComponent
            ],
            providers: [
                finalForm_resolver_service_1.FinalFormResolver,
                editor_resolver_service_1.EditorResolver,
                tag_resolver_service_1.TagResolver,
                linkList_resolver_service_1.LinkListResolver
            ]
        })
    ], LinkModule);
    return LinkModule;
}());
exports.LinkModule = LinkModule;


/***/ }),

/***/ "./ClientApp/app/link/linkList-resolver.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var LinkListResolver = /** @class */ (function () {
    function LinkListResolver(service) {
        this.service = service;
    }
    LinkListResolver.prototype.resolve = function (route, state) {
        return this.service.loadLinks();
    };
    LinkListResolver = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [dataService_1.DataService])
    ], LinkListResolver);
    return LinkListResolver;
}());
exports.LinkListResolver = LinkListResolver;


/***/ }),

/***/ "./ClientApp/app/link/linkList.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-6 col-md-offset-3\">\r\n    <div id=\"addLink\" class=\"row\">       \r\n        <form novalidate> \r\n            <div class=\"input-group\">\r\n                <input #urlInput=\"ngModel\" class=\"form-control\" type=\"url\" name=\"text\" [(ngModel)]='url' \r\n                       pattern=\"(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]\\.[^\\s]{2,})\" \r\n                       placeholder=\"Paste or enter url\" required />\r\n                <span class=\"input-group-btn\"><button [disabled]=\"urlInput.invalid\" type=\"submit\" class=\"btn btn-success\" [routerLink]=\"['/finalForm']\" [queryParams]=\"{url: url}\">Add</button></span>\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-md-2\">Filter by:</div>\r\n        <div class=\"col-md-4\">\r\n            <input type=\"text\" [(ngModel)]='listFilter' />\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n            <h3>Filtered by: {{ listFilter }}</h3>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"isBusy\" class=\"text-center\"><i class=\"fa fa-spinner fa-spin\"></i>Loading</div>\r\n    <table class=\"table table-responsive table-striped\">\r\n        <tr *ngFor=\"let link of filteredLinks\">\r\n            <td>{{ link.name }}</td>\r\n            <td>{{ link.dateCreated | date:'short'}}</td>\r\n            <td><a href=\"{{ link.url }}\" target=\"_blank\">{{ link.url }}</a></td>\r\n            <td><a [routerLink]=\"[link.name,'edit']\" [queryParams]=\"{filterBy: listFilter}\" class=\"btn btn-sm btn-primary\">Edit</a></td>\r\n            <td><a [routerLink]=\"[link.name,'tags']\" id=\"tagButton\" class=\"btn btn-primary fa fa-tag\"></a></td>\r\n            <td><a class=\"fa fa-remove text-danger\" (click)=\"deleteLink(link.name)\"></a></td>\r\n        </tr>\r\n    </table>\r\n</div>"

/***/ }),

/***/ "./ClientApp/app/link/linkList.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var LinkListComponent = /** @class */ (function () {
    function LinkListComponent(data, route, router) {
        this.data = data;
        this.route = route;
        this.router = router;
        this.isBusy = true;
    }
    Object.defineProperty(LinkListComponent.prototype, "listFilter", {
        get: function () {
            return this._listFilter;
        },
        set: function (value) {
            this._listFilter = value;
            this.filteredLinks = this.listFilter ? this.performFilter(this.listFilter) : this.links;
        },
        enumerable: true,
        configurable: true
    });
    LinkListComponent.prototype.performFilter = function (filterBy) {
        filterBy = filterBy.toLocaleLowerCase();
        return this.links.filter(function (link) {
            return link.name.toLocaleLowerCase().indexOf(filterBy) !== -1;
        });
    };
    LinkListComponent.prototype.deleteLink = function (name) {
        var _this = this;
        this.data.deleteLink(encodeURIComponent(name))
            .subscribe(function (data) {
            _this.ngOnInit();
        });
    };
    ;
    LinkListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.loadLinks()
            .subscribe(function (links) {
            _this.links = links;
            _this.filteredLinks = _this.links;
            _this.filteredLinks = _this.filteredLinks.reverse();
            _this.listFilter = _this.route.snapshot.queryParams['filterBy'] || '';
            _this.isBusy = false;
        }, function (error) { return _this.errorMessage = error; });
        // TO DO this section 
        this.route.data.map(function (data) { return data.linkList; }).subscribe(function (links) {
            _this.links = links;
            _this.filteredLinks = _this.links;
            _this.filteredLinks = _this.filteredLinks.reverse();
            _this.listFilter = _this.route.snapshot.queryParams['filterBy'] || '';
            _this.isBusy = false;
        });
    };
    LinkListComponent = __decorate([
        core_1.Component({
            selector: "link-list",
            template: __webpack_require__("./ClientApp/app/link/linkList.component.html"),
            styleUrls: []
        }),
        __metadata("design:paramtypes", [dataService_1.DataService, router_1.ActivatedRoute, router_1.Router])
    ], LinkListComponent);
    return LinkListComponent;
}());
exports.LinkListComponent = LinkListComponent;


/***/ }),

/***/ "./ClientApp/app/page-not-found.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = __decorate([
        core_1.Component({
            selector: 'app-page-not-found',
            template: "\n    <p>\n      Page not found.\n    </p>\n  ",
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());
exports.PageNotFoundComponent = PageNotFoundComponent;


/***/ }),

/***/ "./ClientApp/app/shared/dataService.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var rxjs_1 = __webpack_require__("./node_modules/rxjs/Rx.js");
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
__webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
__webpack_require__("./node_modules/rxjs/_esm5/add/observable/throw.js");
__webpack_require__("./node_modules/rxjs/_esm5/add/operator/catch.js");
__webpack_require__("./node_modules/rxjs/_esm5/add/operator/do.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var DataService = /** @class */ (function () {
    function DataService(http, route) {
        this.http = http;
        this.route = route;
    }
    // Load links on landing page
    DataService.prototype.loadLinks = function () {
        return this.http.get("/api/links")
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    DataService.prototype.handleError = function (error) {
        console.log(error.message);
        return rxjs_1.Observable.throw(error.message);
    };
    // Load link by name and username
    DataService.prototype.loadLinkByName = function (name) {
        return this.http.get("/api/links/" + name);
    };
    // Load link name, link url and tags from API
    DataService.prototype.loadData = function (url) {
        return this.http.get("/api/finalform?url=" + url);
    };
    // Load tags of given link
    DataService.prototype.loadTags = function (name) {
        return this.http.get("/api/links/" + name + "/tags");
    };
    // Post data to server
    DataService.prototype.postData = function (data) {
        //console.log('data: ', data);
        //let header = new HttpHeaders();
        //header.append('Content-Type', 'application/json;charset=utf-8')
        return this.http.post("/api/links", data, {
            headers: new http_1.HttpHeaders({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    };
    // Update link 
    DataService.prototype.updateLink = function (name, data) {
        return this.http.put("/api/links/" + name, data, {
            headers: new http_1.HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    };
    // Delete link
    DataService.prototype.deleteLink = function (name) {
        return this.http.delete("/api/links/" + name);
    };
    DataService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.ActivatedRoute])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;


/***/ }),

/***/ "./ClientApp/app/tag/tag-resolver.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var TagResolver = /** @class */ (function () {
    function TagResolver(service) {
        this.service = service;
    }
    TagResolver.prototype.resolve = function (route, state) {
        var name = encodeURIComponent(route.params['linkName']);
        return this.service.loadTags(name);
    };
    TagResolver = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [dataService_1.DataService])
    ], TagResolver);
    return TagResolver;
}());
exports.TagResolver = TagResolver;


/***/ }),

/***/ "./ClientApp/app/tag/tag.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-3\">\r\n    <div><a [routerLink]=\"['/']\" queryParamsHandling=\"preserve\" class=\"btn btn-group-sm btn-default\"><i class=\"fa fa-angle-left\"></i> Back</a></div>\r\n    <table class=\"table table-responsive table-striped\">\r\n        <tr>\r\n            <th class=\"text-primary\">{{ title }} - tags</th>\r\n        </tr>\r\n        <tr *ngFor=\"let tag of tags\">\r\n            <td>{{ tag.name }}</td>\r\n        </tr>\r\n    </table>\r\n</div>"

/***/ }),

/***/ "./ClientApp/app/tag/tag.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var dataService_1 = __webpack_require__("./ClientApp/app/shared/dataService.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var TagComponent = /** @class */ (function () {
    function TagComponent(data, route, http) {
        this.data = data;
        this.route = route;
        this.http = http;
        this.tags = [];
    }
    TagComponent.prototype.ngOnInit = function () {
        //let name = this.route.snapshot.paramMap.get('linkName');
        //this.data.loadTags(name)
        //    .subscribe(tags => {
        //        this.tags = tags;
        //    });
        this.title = this.route.snapshot.params['linkName'];
        this.tags = this.route.snapshot.data['tags'];
    };
    TagComponent = __decorate([
        core_1.Component({
            selector: 'app-tag',
            template: __webpack_require__("./ClientApp/app/tag/tag.component.html")
        }),
        __metadata("design:paramtypes", [dataService_1.DataService, router_1.ActivatedRoute, http_1.HttpClient])
    ], TagComponent);
    return TagComponent;
}());
exports.TagComponent = TagComponent;


/***/ }),

/***/ "./ClientApp/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "./ClientApp/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("./ClientApp/app/app.module.ts");
var environment_1 = __webpack_require__("./ClientApp/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./ClientApp/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map