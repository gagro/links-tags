"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dataService_1 = require("../shared/dataService");
var router_1 = require("@angular/router");
var http_1 = require("@angular/common/http");
var router_2 = require("@angular/router");
var EditorComponent = /** @class */ (function () {
    function EditorComponent(data, router, route, http) {
        this.data = data;
        this.router = router;
        this.route = route;
        this.http = http;
        this.errorMessage = "";
        this.isBusy = true;
    }
    EditorComponent.prototype.updateLink = function () {
        var _this = this;
        this.data.updateLink(this.title, this.link)
            .subscribe(function (data) {
            return _this.router.navigate(["/"]);
        });
    };
    EditorComponent.prototype.deleteLink = function () {
        var _this = this;
        this.data.deleteLink(encodeURIComponent(this.link.name))
            .subscribe(function (data) {
            return _this.router.navigate(["/"]);
        });
    };
    ;
    EditorComponent.prototype.ngOnInit = function () {
        //let name = this.route.snapshot.paramMap.get('linkName');
        //this.data.loadTags(name)
        //    .subscribe(tags => {
        //        this.tags = tags;
        //    });
        this.title = this.route.snapshot.params['linkName'];
        this.link = this.route.snapshot.data['link'];
    };
    EditorComponent = __decorate([
        core_1.Component({
            selector: "editor",
            templateUrl: "editor.component.html",
            styleUrls: ['editor.component.css']
        }),
        __metadata("design:paramtypes", [dataService_1.DataService, router_2.Router, router_1.ActivatedRoute, http_1.HttpClient])
    ], EditorComponent);
    return EditorComponent;
}());
exports.EditorComponent = EditorComponent;
//# sourceMappingURL=editor.component.js.map