"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
var core_1 = require("@angular/core");
require("rxjs/add/operator/map");
require("rxjs/add/observable/throw");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/do");
var router_1 = require("@angular/router");
var DataService = /** @class */ (function () {
    function DataService(http, route) {
        this.http = http;
        this.route = route;
    }
    // Load links on landing page
    DataService.prototype.loadLinks = function () {
        return this.http.get("/api/links")
            .do(function (data) { return console.log('All: ' + JSON.stringify(data)); })
            .catch(this.handleError);
    };
    DataService.prototype.handleError = function (error) {
        console.log(error.message);
        return rxjs_1.Observable.throw(error.message);
    };
    // Load link by name and username
    DataService.prototype.loadLinkByName = function (name) {
        return this.http.get("/api/links/" + name);
    };
    // Load link name, link url and tags from API
    DataService.prototype.loadData = function (url) {
        return this.http.get("/api/finalform?url=" + url);
    };
    // Load tags of given link
    DataService.prototype.loadTags = function (name) {
        return this.http.get("/api/links/" + name + "/tags");
    };
    // Post data to server
    DataService.prototype.postData = function (data) {
        //console.log('data: ', data);
        //let header = new HttpHeaders();
        //header.append('Content-Type', 'application/json;charset=utf-8')
        return this.http.post("/api/links", data, {
            headers: new http_1.HttpHeaders({
                'Content-Type': 'application/json; charset=utf-8'
            })
        });
    };
    // Update link 
    DataService.prototype.updateLink = function (name, data) {
        return this.http.put("/api/links/" + name, data, {
            headers: new http_1.HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    };
    // Delete link
    DataService.prototype.deleteLink = function (name) {
        return this.http.delete("/api/links/" + name);
    };
    DataService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.ActivatedRoute])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
//# sourceMappingURL=dataService.js.map