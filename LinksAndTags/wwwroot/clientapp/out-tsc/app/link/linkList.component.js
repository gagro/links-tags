"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dataService_1 = require("../shared/dataService");
var router_1 = require("@angular/router");
var LinkListComponent = /** @class */ (function () {
    function LinkListComponent(data, route, router) {
        this.data = data;
        this.route = route;
        this.router = router;
        this.isBusy = true;
    }
    Object.defineProperty(LinkListComponent.prototype, "listFilter", {
        get: function () {
            return this._listFilter;
        },
        set: function (value) {
            this._listFilter = value;
            this.filteredLinks = this.listFilter ? this.performFilter(this.listFilter) : this.links;
        },
        enumerable: true,
        configurable: true
    });
    LinkListComponent.prototype.performFilter = function (filterBy) {
        filterBy = filterBy.toLocaleLowerCase();
        return this.links.filter(function (link) {
            return link.name.toLocaleLowerCase().indexOf(filterBy) !== -1;
        });
    };
    LinkListComponent.prototype.deleteLink = function (name) {
        var _this = this;
        this.data.deleteLink(encodeURIComponent(name))
            .subscribe(function (data) {
            _this.ngOnInit();
        });
    };
    ;
    LinkListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.loadLinks()
            .subscribe(function (links) {
            _this.links = links;
            _this.filteredLinks = _this.links;
            _this.filteredLinks = _this.filteredLinks.reverse();
            _this.listFilter = _this.route.snapshot.queryParams['filterBy'] || '';
            _this.isBusy = false;
        }, function (error) { return _this.errorMessage = error; });
        // TO DO this section 
        this.route.data.map(function (data) { return data.linkList; }).subscribe(function (links) {
            _this.links = links;
            _this.filteredLinks = _this.links;
            _this.filteredLinks = _this.filteredLinks.reverse();
            _this.listFilter = _this.route.snapshot.queryParams['filterBy'] || '';
            _this.isBusy = false;
        });
    };
    LinkListComponent = __decorate([
        core_1.Component({
            selector: "link-list",
            templateUrl: "linkList.component.html",
            styleUrls: []
        }),
        __metadata("design:paramtypes", [dataService_1.DataService, router_1.ActivatedRoute, router_1.Router])
    ], LinkListComponent);
    return LinkListComponent;
}());
exports.LinkListComponent = LinkListComponent;
//# sourceMappingURL=linkList.component.js.map