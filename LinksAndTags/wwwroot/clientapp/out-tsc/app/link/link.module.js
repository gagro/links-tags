"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var linkList_component_1 = require("../link/linkList.component");
var link_component_1 = require("./link.component");
var editor_component_1 = require("../editor/editor.component");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var finalForm_comptonent_1 = require("../finalForm/finalForm.comptonent");
var finalForm_resolver_service_1 = require("../finalForm/finalForm-resolver.service");
var editor_resolver_service_1 = require("../editor/editor-resolver.service");
var link_edit_info_component_1 = require("../editor/link-edit-info.component");
var link_edit_tags_component_1 = require("../editor/link-edit-tags.component");
var tag_component_1 = require("../tag/tag.component");
var tag_resolver_service_1 = require("../tag/tag-resolver.service");
var linkList_resolver_service_1 = require("./linkList-resolver.service");
var LinkModule = /** @class */ (function () {
    function LinkModule() {
    }
    LinkModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                router_1.RouterModule.forChild([
                    { path: '', component: link_component_1.LinkComponent, resolve: { linkList: linkList_resolver_service_1.LinkListResolver }, pathMatch: 'full' },
                    {
                        path: ":linkName/edit",
                        component: editor_component_1.EditorComponent,
                        resolve: { link: editor_resolver_service_1.EditorResolver },
                        children: [
                            {
                                path: '', redirectTo: 'info', pathMatch: 'full'
                            },
                            {
                                path: 'info', component: link_edit_info_component_1.LinkEditInfoComponent
                            },
                            {
                                path: 'tags', component: link_edit_tags_component_1.LinkEditTagsComponent
                            }
                        ]
                    },
                    { path: 'finalForm', component: finalForm_comptonent_1.FinalFormComponent, resolve: { finalData: finalForm_resolver_service_1.FinalFormResolver } },
                    { path: ':linkName/tags', component: tag_component_1.TagComponent, resolve: { tags: tag_resolver_service_1.TagResolver } }
                ])
            ],
            declarations: [
                linkList_component_1.LinkListComponent,
                link_component_1.LinkComponent,
                editor_component_1.EditorComponent,
                link_edit_info_component_1.LinkEditInfoComponent,
                link_edit_tags_component_1.LinkEditTagsComponent
            ],
            providers: [
                finalForm_resolver_service_1.FinalFormResolver,
                editor_resolver_service_1.EditorResolver,
                tag_resolver_service_1.TagResolver,
                linkList_resolver_service_1.LinkListResolver
            ]
        })
    ], LinkModule);
    return LinkModule;
}());
exports.LinkModule = LinkModule;
//# sourceMappingURL=link.module.js.map