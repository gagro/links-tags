"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dataService_1 = require("../shared/dataService");
var router_1 = require("@angular/router");
var FinalFormComponent = /** @class */ (function () {
    function FinalFormComponent(data, route, router) {
        this.data = data;
        this.route = route;
        this.router = router;
        this.errorMessage = "";
        this.isBusy = true;
        this.checkboxTag = [];
    }
    FinalFormComponent.prototype.CheckBoxValueChange = function (isChecked, checkboxValue) {
        if (isChecked) {
            this.checkboxTag.push(checkboxValue);
        }
        else {
            this.checkboxTag.splice(this.checkboxTag.indexOf(checkboxValue), 1);
        }
    };
    FinalFormComponent.prototype.submitForm = function () {
        var _this = this;
        this.finalData.tags = this.checkboxTag;
        if (this.newTags != undefined) {
            var inputTags = this.newTags.split(',');
            for (var _i = 0, inputTags_1 = inputTags; _i < inputTags_1.length; _i++) {
                var i = inputTags_1[_i];
                i = i.trim();
            }
            this.finalData.tags = this.finalData.tags.concat(inputTags);
        }
        this.data.postData(this.finalData)
            .subscribe(function (data) {
            _this.finalData = data;
            _this.router.navigate(["/"]);
        }, function (err) { return _this.errorMessage = err; });
    };
    FinalFormComponent.prototype.ngOnInit = function () {
        //let url = encodeURIComponent(this.route.snapshot.queryParams['url']);
        //this.data.loadData(url)
        //    .subscribe(finalData => {
        //        this.finalData = finalData;
        //    }); 
        this.finalData = this.route.snapshot.data['finalData'];
    };
    FinalFormComponent = __decorate([
        core_1.Component({
            selector: "final-form",
            templateUrl: "finalForm.component.html"
        }),
        __metadata("design:paramtypes", [dataService_1.DataService, router_1.ActivatedRoute, router_1.Router])
    ], FinalFormComponent);
    return FinalFormComponent;
}());
exports.FinalFormComponent = FinalFormComponent;
//# sourceMappingURL=finalForm.comptonent.js.map