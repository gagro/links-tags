"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dataService_1 = require("../shared/dataService");
var router_1 = require("@angular/router");
var http_1 = require("@angular/common/http");
var TagComponent = /** @class */ (function () {
    function TagComponent(data, route, http) {
        this.data = data;
        this.route = route;
        this.http = http;
        this.tags = [];
    }
    TagComponent.prototype.ngOnInit = function () {
        //let name = this.route.snapshot.paramMap.get('linkName');
        //this.data.loadTags(name)
        //    .subscribe(tags => {
        //        this.tags = tags;
        //    });
        this.title = this.route.snapshot.params['linkName'];
        this.tags = this.route.snapshot.data['tags'];
    };
    TagComponent = __decorate([
        core_1.Component({
            selector: 'app-tag',
            templateUrl: "tag.component.html"
        }),
        __metadata("design:paramtypes", [dataService_1.DataService, router_1.ActivatedRoute, http_1.HttpClient])
    ], TagComponent);
    return TagComponent;
}());
exports.TagComponent = TagComponent;
//# sourceMappingURL=tag.component.js.map