﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinksAndTags.ViewModels
{
    public class FinalFormViewModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.UtcNow;
        public List<string> Tags { get; set; }
    }
}
