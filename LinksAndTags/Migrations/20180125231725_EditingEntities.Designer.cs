﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using LinksAndTags.Models;

namespace LinksAndTags.Migrations
{
    [DbContext(typeof(LinksAndTagsContext))]
    [Migration("20180125231725_EditingEntities")]
    partial class EditingEntities
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("LinksAndTags.Models.Links", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateCreated");

                    b.Property<string>("Name");

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.ToTable("Links");
                });

            modelBuilder.Entity("LinksAndTags.Models.Tags", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("LinksId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("LinksId");

                    b.ToTable("Tags");
                });

            modelBuilder.Entity("LinksAndTags.Models.Tags", b =>
                {
                    b.HasOne("LinksAndTags.Models.Links")
                        .WithMany("Tags")
                        .HasForeignKey("LinksId");
                });
        }
    }
}
