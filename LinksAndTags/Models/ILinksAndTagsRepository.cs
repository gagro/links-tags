﻿using AngleSharp.Dom;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LinksAndTags.Models
{
    public interface ILinksAndTagsRepository
    {
        IEnumerable<Links> GetAllLinks();
        string GetBody(IDocument html);
        Task<IDocument> GetHtml(string input);
        IEnumerable<Links> GetLinksByUsername(string username);
        Links GetLinkByName(string linkName);
        Links GetUserLinkByName(string linkName, string username);
        string GetTitle(string url);
        List<string> GetTags(IDocument html, List<string> textList);

        string RemoveStopwords(string input);
        string RemovePunctuation(string text);

        void AddLink(Links link);
        void AddTag(Links link, Tags newTag, string username);

        Task<bool> SaveChangesAsync();
        
    }
}