﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinksAndTags.Models
{
    public class LinksAndTagsContextSeedData
    {
        private LinksAndTagsContext _context;
        private UserManager<ApplicationUser> _userManager;

        public LinksAndTagsContextSeedData(LinksAndTagsContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task EnsureSeedData()
        {
            if (await _userManager.FindByEmailAsync("testuser@gmail.com") == null)
            {
                var user = new ApplicationUser()
                {
                    UserName = "testUser",
                    Email = "testuser@gmail.com"
                };

                await _userManager.CreateAsync(user, "P@ssw0rd!");
            }
            if (!_context.Links.Any())
            {
                var verge = new Links()
                {
                    DateCreated = DateTime.UtcNow,
                    Name = "Verge",
                    UserName = "testUser",
                    Url = "https://www.theverge.com/",
                    Tags = new List<Tags>()
                    {
                        new Tags() {Name = "technology"},
                        new Tags() {Name = "pc"},
                        new Tags() {Name = "laptop"},
                        new Tags() {Name = "news"},
                        new Tags() {Name = "techNews"}
                    }
                };

                _context.Links.Add(verge);
                _context.Tags.AddRange(verge.Tags);

                var stackoverflow = new Links()
                {
                    DateCreated = DateTime.UtcNow,
                    Name = "Stackoverflow",
                    UserName = "testUser",
                    Url = "https://stackoverflow.com/",
                    Tags = new List<Tags>()
                    {
                        new Tags() {Name = "learn"},
                        new Tags() {Name = "share"},
                        new Tags() {Name = "build"}
                    }
                };

                _context.Links.Add(stackoverflow);
                _context.Tags.AddRange(stackoverflow.Tags);

                await _context.SaveChangesAsync();
            }
        }
    }
}
