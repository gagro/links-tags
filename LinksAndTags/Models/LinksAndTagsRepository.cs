﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using AngleSharp;
using AngleSharp.Dom;

namespace LinksAndTags.Models
{
    public class LinksAndTagsRepository : ILinksAndTagsRepository
    {
        private LinksAndTagsContext _context;

        public LinksAndTagsRepository(LinksAndTagsContext context)
        {
            _context = context;
        }

        public void AddLink(Links link)
        {
            _context.Add(link);
        }

        public void AddTag(Links link, Tags newTag, string username)
        {
            //var link = GetLinkByName(linkName);

            if (link != null)
            {
                link.Tags.Add(newTag);
                _context.Tags.Add(newTag);
            }
        }

        /// <summary>
        /// Methods for final form data
        /// </summary>
        /// <returns></returns>

        public IEnumerable<Links> GetAllLinks()
        {
            return _context.Links.ToList();
        }

        public string GetBody(IDocument html)
        {
            var body = html.Body;
            //var unvantedTags = RemoveUnwantedTags(body.TextContent);
            var scripts = body.GetElementsByTagName("script");
            foreach (var script in scripts)
            {
                script.Parent.RemoveChild(script);
            }
            var final = Regex.Replace(body.TextContent, @"\s+", " ").Trim();
            return final;
        }

        public async Task<IDocument> GetHtml(string input)
        {
            var config = Configuration.Default.WithDefaultLoader();
            var context = BrowsingContext.New(config);
            return await context.OpenAsync(input);
        }

        public Links GetLinkByName(string linkName)
        {
            return _context.Links
                .Include(t => t.Tags)
                .Where(t => t.Name == linkName)
                .FirstOrDefault();
        }

        public IEnumerable<Links> GetLinksByUsername(string name)
        {
            return _context.Links
                .Include(t => t.Tags)
                .Where(t => t.UserName == name)
                .ToList();
        }

        public List<string> GetTags(IDocument html, List<string> textList)
        {
            var meta = html.All.Where(n => n.LocalName == "meta" && n.OuterHtml.Contains("keywords")).ToList();
            if (meta.Count > 0)
            {
                var content = meta.Select(c => c.GetAttribute("content")).FirstOrDefault().Split(',');
                var tagList = new List<string>();

                foreach (var keyword in content)
                {
                    tagList.Add(keyword.Trim());
                }
                if (tagList.Count > 0)
                {
                    return tagList;
                }
            }

            var dict = new Dictionary<string, int>();
            foreach (var word in textList)
            {
                if (!dict.ContainsKey(word))
                {
                    dict.Add(word, 1);
                }
                else
                {
                    dict[word]++;
                }
            }
            var ordered = dict.OrderByDescending(s => s.Value);
            var list = new List<string>();
            foreach (var item in ordered)
            {
                if (item.Value > 1)
                {
                    if (list.Count == 5)
                    {
                        break;
                    }
                    list.Add(item.Key);
                }
            }
            return list;
        }

        public string GetTitle(string url)
        {
            var content = GetContent(url).Result;
            return Regex.Match(content, @"\<title\b[^>]*\>\s*(?<Title>[\s\S]*?)\</title\>", RegexOptions.IgnoreCase).Groups["Title"].Value;
        }

        public async Task<string> GetContent(string url)
        {
            HttpClient title = new HttpClient();
            return await title.GetStringAsync(url);
        }

        public Links GetUserLinkByName(string linkName, string username)
        {
            return _context.Links
                .Include(t => t.Tags)
                .Where(t => t.Name == linkName && t.UserName == username)
                .FirstOrDefault();
        }        

        /// <summary>
        /// Remove stopwords from string.
        /// </summary>
        public string RemoveStopwords(string input)
        {
            // 1
            // Split parameter into words
            var words = input.Split(StopWords._delimiters,
                StringSplitOptions.RemoveEmptyEntries);
            // 3
            // Store results in this StringBuilder
            StringBuilder builder = new StringBuilder();
            // 4
            // Loop through all words
            foreach (string currentWord in words)
            {
                // 5
                // Convert to lowercase
                string lowerWord = currentWord.ToLower();
                // 6
                // If this is a usable word, add it
                if (!StopWords._stops.ContainsKey(lowerWord))
                {
                    builder.Append(currentWord).Append(' ');
                }
            }
            // 7
            // Return string with words removed
            return builder.ToString().Trim();
        }

        public string RemovePunctuation(string text)
        {
            return new string(text.Where(c => !char.IsPunctuation(c)).ToArray());
        }

        // Save changes to Db

        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }
    }
}
